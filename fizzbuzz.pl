#!/usr/bin/env perl
use strict;
use warnings;

print _fizzbuzz($_), "\n" for (1..100);

sub _fizzbuzz {
  my $int = shift;
  return "FizzBuzz" if $int % 3 == 0 and $int % 5 == 0;
  return "Fizz"     if $int % 3 == 0;
  return "Buzz"     if $int % 5 == 0;
  return $int;
}
